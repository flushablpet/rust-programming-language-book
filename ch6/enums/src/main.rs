// Forms discriminated union / sum type.
enum IpKind {
    V4(u8, u8, u8, u8),
    V6(String)
}

enum Message {
    Quit, // No associated date
    Move { x: i32, y: i32 }, // Anon struct
    Write(String), // Named struct
    Color(u32, u32, u32) // Tuple struct
}


fn main() {
    let home = IpKind::V4(127, 0, 0, 1);
    let loopback = IpKind::V6(String::from("::1"));
    route(home);
    route(loopback);
}

fn route(ip: IpKind) {
    match ip {
        IpKind::V4(a, b, c, d) => println!("Ip is {}.{}.{}.{}", a, b, c, d),
        IpKind::V6(s) => println!("Ip is {}", s)
    };
}