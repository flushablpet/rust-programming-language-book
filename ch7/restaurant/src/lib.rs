
// Declare modules with mod.
mod front_of_house; // src/front_of_house.rs
mod back_of_house; // src/back_of_house.rs

// Import the module in /src/front_of_house/hosting.rs
// specify an absolute path with 'crate::'
pub use front_of_house::hosting;

// this fn can access the private front_of_house because they are siblings.
// fns external to this file could not.
pub fn dine() {
    // Call fn in front_of_house::hosting module
    hosting::add_to_waitlist();
}

