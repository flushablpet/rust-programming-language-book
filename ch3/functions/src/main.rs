fn main() {
    let x = 2;
    another_function(x,4);
    expression_block();
    let x = return_values(2);
    println!("return_values: {}", x);
}

fn another_function(x: i32, y: i32) {
    println!("x: {}, y: {}", x, y);
}

/// Shows how an expression is returned as a result from a statement block
fn expression_block() {
    // Inner expression is passed out and assigned to z
    let z = {
        let x = 5;
        // More statements here...
        x + x*x // No semi-colon makes this last command a expression, not a statement
    };
    println!("z: {}", z);
}

/// Return type is set in the function signature by -> T
fn return_values(x: i32) -> i32 {
    if x % 2 == 0 {
        // return early with explicit return keyword
        return x + 1;
    }

    x*x
}