
fn main() {
    mutability();
    shadowing();
}

/// Mark a variable with mut to overwrite after declaration
fn mutability() {
    println!("Mutability");
    // let x = 5; // Will fail since it is immutable by default
    let mut x = 5;
    println!("{}", x);
    x = 6;
    println!("{}", x);
}

/// Shadowing creates a new variable using an existing variable name. The type can change.
fn shadowing() {
    println!("Shadowing");
    let x = 5;
    println!("{}", x);
    // Changing the type when shadowing
    let x = x as f32 + 1.0;
    println!("{}", x);
}