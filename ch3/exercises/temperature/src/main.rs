use std::io::{self, Write};
pub mod temperature;

fn main() {
    println!("Temperature Conversion!");

    loop {
        let mut kind = String::new();
        get_type(&mut kind);

        let kind: temperature::Kind = match kind.trim() {
            "c" | "C" => temperature::Kind::Celcius,
            "f" | "F" => temperature::Kind::Fahrenheit,
            "q" | "Q" => break,
            _ => continue
        };

        let mut temp = String::new();
        get_value(&mut temp, &kind);

        let temp: temperature::Temp = match temp.trim().parse() {
            Ok(value) => temperature::Temp::new(value, kind),
            Err(_) => continue
        };

        temp.display_temp();
    }

}

fn get_type(temp_type: &mut String) {
    print!("Convert from (C)elcius, (F)ahrenheit, (Q)uit: ");
    io::stdout().flush().unwrap();
    io::stdin()
            .read_line(temp_type)
            .expect("Unable to read stdin");
}

fn get_value(temp: &mut String, kind: &temperature::Kind ) {
    print!("Enter {:?} value: ", kind);
    io::stdout().flush().unwrap();
    io::stdin()
            .read_line(temp)
            .expect("Unable to read stdin");
}