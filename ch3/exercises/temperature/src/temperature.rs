
#[derive(Debug)]
pub enum Kind {
    Celcius,
    Fahrenheit
}

pub struct Temp {
    kind: Kind,
    value: f32
}

trait Convert {
    const THIRTY_TWO_F: f32 = 32.;
    const FIVE_NINE : f32 = 5./9.;
    const NINE_FIVE: f32 = 1.8;
    fn convert(&self) -> f32;
    fn f_to_c(f: f32) -> f32;
    fn c_to_f(c: f32) -> f32;
}

impl Convert for Temp {
    fn convert(&self) -> f32 {
        match self.kind {
            Kind::Celcius => Temp::c_to_f(self.value),
            Kind::Fahrenheit => Temp::f_to_c(self.value)
        }
    }

    fn f_to_c(f: f32) -> f32 {
        (f - Temp::THIRTY_TWO_F) * Temp::FIVE_NINE
    }

    fn c_to_f(c: f32) -> f32 {
        (c * Temp::NINE_FIVE) + Temp::THIRTY_TWO_F
    }
}

impl Temp {
    pub fn new(value: f32, kind: Kind) -> Self {
        Self { kind, value }
    }

    pub fn display_temp(&self) {
        let t = self.convert();
        match self.kind {
            Kind::Celcius =>
                println!("Result: {:.2}C → {:.1}F", self.value, t),
            Kind::Fahrenheit =>
                println!("Result: {:.1}F → {:.2}C", self.value, t)
        };
    }
}
