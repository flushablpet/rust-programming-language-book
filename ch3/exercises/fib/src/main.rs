use std::io::{self, Write};

/// Prompts the user for a positive integer, n
/// Returns the nth Fib number.
/// fib(n) = fib(n-1) + fib(n-2)
fn main() {
    loop {
        let mut input = String::new();
        get_input(&mut input);

        let n: u64 = match input.trim().parse() {
            Ok(n) => n,
            Err(_) => continue
        };

        let f = fib(n);
        println!("{}th Fib = {}", n, f);
    }
}

/// T = O(n), S = O(1)
fn fib(n: u64) -> u64 {
    // Locally hold the last two known values. Prefill with vals for n= 0, 1.
    let mut memo = (0, 1);
    if n == 0 {
        0
    } else {
        for _ in 1..n {
           memo = (memo.1, memo.0 + memo.1);
        }
    memo.1
    }
}

fn get_input(input: &mut String) {
    print!("Enter a positive integer: ");
    io::stdout().flush().unwrap();
    io::stdin()
        .read_line(input)
        .expect("Failed to read stdin");
}