fn main() {
    println!("if_expression: {}", if_expression(true));
    println!("loop_expression: {}", loop_expression());
    println!("whiles: {}", whiles(5));
    array_iter();
    for_range();
}

fn if_expression(condition: bool) -> i32 {
    // If expressions can assign to a variable or directly return
    let x = if condition {
        5
    } else {
        6
    };

    // Here the if expression returns out of the function
    if x < 6 {
        x + 1
    } else {
        x * x
    }
}

fn loop_expression() -> i32 {
    let mut ct = 0;
    // Directly return the loop expression as a result
    loop {
        if ct == 10 {
            break ct * 2; // returns the computation from the expression
        }
        ct += 1;
    }
}

fn whiles(n: i32) -> i32 {
    let mut ct = 0;

    while ct < n {
        print!("{} ", ct);
        ct += 1;
    }
    ct
}

fn array_iter() {
    let a = [2, 4, 8, 16, 32, 64];

    for e in a.iter() {
        println!("{}", e);
    }
}

/// Range type generates vals from start to end, exclusive.
/// Adding = to (start, =end) makes the range inclusive.
fn for_range() {
    for e in (1..5).rev() {
        println!("{}", e);
    }
    println!("Done!");
}