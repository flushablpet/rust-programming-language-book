//! The Rust Programming Book : Chapter 2
use std::io;
use rand::Rng;
use std::cmp::Ordering;

fn main() {
    println!("Guessing Game!");
    let secret = gen_secret_number();
    guess(&secret);
    println!("Game over!");
}

/// Guessing Game
fn guess(secret: &u32) {
    loop {
        println!("Guess a number between 1 and 100");
        let mut guess = String::new();
        io::stdin()
            .read_line(&mut guess)
            .expect("Unable to read from stdin");

        let guess:u32 = match guess.trim().parse() {
            Ok(n) => n,
            Err(_) => continue
        };

        println!("Your guess was {}", guess);
        match guess.cmp(&secret) {
            Ordering::Less => println!("Too small"),
            Ordering::Greater => println!("Too big"),
            Ordering::Equal => {
                println!("Bingo!");
                break;
            }
        }
    }
}

/// Create a random number between 1 and 100
fn gen_secret_number()-> u32 {
    rand::thread_rng().gen_range(1..=100)
}