#[derive(Debug)] // Allows printing of structure to stdout
struct Rectangle {
    w: u32,
    h: u32
}

impl Rectangle {
    // self binds the fn as a method onto instances of the struct
    fn area(&self) -> u32 {
        self.w * self.h
    }

    fn can_contain(&self, other: &Rectangle) -> bool {
        self.w > other.w && self.h > other.h
    }

    // Assoc fn bound to struct type itself
    fn square(size: u32) -> Rectangle {
        Rectangle { w: size, h: size }
    }
}

fn main() {
    let rect = Rectangle { w: 30, h: 50 };
    let rect2 = Rectangle::square(25);

    println!(
        "Area of rectangle {:#?} is {} sq pixels.",
        rect, rect.area()
    );

    println!(
        "Rectangle {:#?} can contain other {:#?}: {}",
        rect, rect2, rect.can_contain(&rect2)
    );
}
